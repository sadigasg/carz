package com.tjv.Carz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.tjv.Carz")
public class CarzApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarzApplication.class, args);
	}

}
