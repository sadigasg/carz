package com.tjv.Carz.model.garage;

import com.tjv.Carz.model.car.Car;
import com.tjv.Carz.model.car.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GarageDao {

    @Autowired
    private GarageRepository garageRepository;

    public void save(Garage garage) {
        garageRepository.save(garage);
    }

    public void delete(Garage garage) {
        garageRepository.delete(garage);
    }

    public List<Garage> getAllGarages() {
        List<Garage> garageList = new ArrayList<>();
        Streamable.of(garageRepository.findAll())
                .forEach(garageList::add);
        return garageList;
    }
}
