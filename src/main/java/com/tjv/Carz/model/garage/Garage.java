package com.tjv.Carz.model.garage;

import com.tjv.Carz.model.car.Car;
import com.tjv.Carz.model.owner.Owner;
import org.hibernate.mapping.Set;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "garages")
public class Garage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "garage_cars",
            joinColumns = { @JoinColumn(name = "garage_id") },
            inverseJoinColumns = { @JoinColumn(name = "car_id") })
    private List<Car> cars;

}
