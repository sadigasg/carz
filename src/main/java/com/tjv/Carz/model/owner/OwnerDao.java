package com.tjv.Carz.model.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OwnerDao {

    @Autowired
    private OwnerRepository ownerRepository;

    public void save(Owner owner) {
        ownerRepository.save(owner);
    }

    public void delete(Owner owner) {
        ownerRepository.delete(owner);
    }

    public List<Owner> getAllOwners() {
        List<Owner> ownerList = new ArrayList<>();
        Streamable.of(ownerRepository.findAll())
                .forEach(ownerList::add);
        return ownerList;
    }
}
