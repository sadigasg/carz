package com.tjv.Carz.model.owner;

import com.tjv.Carz.model.garage.Garage;

import javax.persistence.*;
import java.util.List;

@Entity
public class Owner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String surname;

}
