package com.tjv.Carz.model.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarDao {

    @Autowired
    private CarRepository carRepository;

    public void save(Car car) {
        carRepository.save(car);
    }

    public void delete(Car car) {
        carRepository.delete(car);
    }

    public List<Car> getAllCars() {
        List<Car> carList = new ArrayList<>();
        Streamable.of(carRepository.findAll())
                .forEach(carList::add);
        return carList;
    }
}
