package com.tjv.Carz.model.car;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tjv.Carz.model.garage.Garage;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "color")
    private String color;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "cars")
    @JsonIgnore
    private List<Garage> existsInGarages;

}
